To Create A New User
====================

	sudo su postgres
	createuser -D -P testuser
	password


Give User a Password
====================

	ALTER USER postgres with password 'password';

# Test with python

	import psycopg2

	db = psycopg2.connect("host=localhost port=5432 dbname=testdb user=postgres password=password")