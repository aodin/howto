Dump and Restore
================

	sudo -u postgres pg_dump -f /abs/path/of/destination -Fc dbname

# Drop the previous DB first!

	sudo -u postgres pg_restore -C -d postgres -j <#_jobs> <dump_file>

*TODO silent mode for pg_restore? Seems silent unless errors?*
*TODO any way to change destination database?*


Using rsync
===========

	rsync -v -e 'ssh -i /abs/path/to/identity.pem -l ubuntu' ###.###.###.###:/abs/path/of/source /abs/path/to/destination

*TODO wasn't much better than scp*
