Install PostGres 9.2
====================

Use the [PPA from Martin Pitt](https://launchpad.net/~pitti/+archive/postgresql)

Install using:

	sudo add-apt-repository ppa:pitti/postgresql 
	sudo apt-get update
	sudo apt-get install postgresql-9.2


#### References:
[askubuntu](http://askubuntu.com/a/186612)