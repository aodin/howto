Install Phantom.js
==================

To build from source (may take longer than 30 minutes):

    sudo apt-get install build-essential chrpath git-core libssl-dev libfontconfig1-dev
    git clone git://github.com/ariya/phantomjs.git
    cd phantomjs
    git checkout 1.8
    ./build.sh

[From phantomjs](http://phantomjs.org/build.html)

To install the downloaded binary, copy a symbolic link to `usr/local/bin`:

    sudo ln -s /home/mint/sources/phantomjs-1.8.1-linux-x86_64/bin/phantomjs /usr/local/bin/phantomjs

