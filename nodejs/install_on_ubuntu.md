Install Nodejs on Ubuntu
========================

As of nodejs v0.10.0, the package `npm` no longer exists, everything is contained in the `nodejs` package:

    sudo add-apt-repository ppa:chris-lea/node.js
    sudo apt-get update
    sudo apt-get install nodejs


[Stackoverflow link](http://stackoverflow.com/a/7214700)