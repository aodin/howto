Uglifyjs Options
================

Install `uglifyjs` from `npm` globally, using:

    npm install -g uglify-js


Fairly good output with `compress` and `mangle`. Remember to reserve output variables:

    uglifyjs jquery-1.9.1.js -o jquery.min.js -c -m -r '$,require,exports'


Minimum breaking:

    uglifyjs castle-graphs.js -o castle-graphs.min.js -c