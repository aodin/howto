Add SSH Identity File
=====================

To use the pem file I generally add the pem to the ssh agent, then simply refer to the username and host:

    ssh-add ~/.ssh/ec2key.pem
    fab -H ubuntu@ec2-host deploy

or specify the env information (without the key) like the example you linked to:

    env.user = 'ubuntu'
    env.hosts = [
        'ec2-host'
    ]

and run as normal:

    fab deploy

http://stackoverflow.com/a/5072396

Adding this file will allow you to replace the command `ssh -i /path/to/identity.pem user@host` with `ssh user@host`