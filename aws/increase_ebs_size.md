Increase EBS Size
=================

Launching an Amazon EBS-backed instance with increased root device storage disk size

1. Select an Amazon EBS-backed AMI to launch your instance from.
2. Check the root device size and note the AMI ID.
3. Using the command line interface, launch the instance by specifying the AMI ID and the mapping of the root device with the increased size.
4. Connect to the instance.
5. Check the size of the root device on the instance. The increased size of the root device is not apparent yet. This is because the file system does not recognize the increased size on the root device.
6. Resize the file system.
7. Check the size of the root device.
8. The root device of the newly launched instance now shows the increased size.

Note
You cannot decrease the size of your root device to less than the size of the AMI. To decrease the size of your root device, create your own AMI with the desired size for the root device and then launch an instance from that AMI.
Increasing the size of the root device on an Amazon EBS-backed running instance

1. Get the Amazon EBS volume ID and the Availability Zone of a running instance for which you want to increase the root storage size.
2. Stop the instance.
3. Detach the original volume from the instance.
4. Create a snapshot of the detached volume.
5. Create a new volume from the snapshot by specifying a larger size.
6. Attach the new volume to the stopped instance.
7. Start the instance and get the new IP address/hostname.
8. Connect to the instance using the new IP address/hostname.
9. Resize the root file system to the extent of the new Amazon EBS volume.
10. Check the size of the root device. The root device now shows the increased size.
11. [Optional] Delete the old EBS volume, if you no longer need it.



http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/RootDeviceStorage.html

http://stackoverflow.com/questions/9604337/how-do-i-increase-the-ebs-volume-size-of-a-running-instance

http://www.e-zest.net/blog/simple-steps-to-change-size-of-ebs-volume-in-ec2-of-aws-using-aws-console/