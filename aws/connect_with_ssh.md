Using an Identity File (*.pem)
===============================

	ssh -i /abs/path/to/identity.pem ubuntu@###.###.###.###

Secure Copy with Identity
=========================

With destination on the remote server:

	scp -i /abs/path/to/identity.pem /abs/path/of/source  ubuntu@###.###.###.###:/abs/path/to/destination


Rsync with Identity
===================

With destination as the local system:

	rsync -v -e 'ssh -i /abs/path/to/identity.pem -l ubuntu' ###.###.###.###:/abs/path/of/source /abs/path/to/destination