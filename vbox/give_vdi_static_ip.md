Give VirtualBox VDI a Static IP
===============================

The global VirtualBox network settings can be edited at `File > Preferences > Network`

During installation of the instance OS, make sure an SSH-server is installed (or do so afterwards).

In the instance-specific settings, leave its first network adapter as NAT. This adapter will provide internet access for the instance.

Create a second network adapter and set it to `Host-only networking` (or `Host-only adapter`). This adapter will allow SSH access from the host system. Using the global network setting, edit the file `/etc/network/interfaces` to have a second ethernet adapter below the `lo` (localhost) and `eth0` (which corresponds to the NAT connection) devices.

You can add the static IP like this:

    auto eth1
    iface eth1 inet static
        address 192.168.56.10
        netmask 255.255.255.0

From [http://superuser.com/a/357121]

To see initialized network devices:

    ls /sys/class/net

The network adapters can be brought down and up with:

    ifdown -a
    ifup -a