Start VirtualBox VDI Headless
=============================

List available virtual images

    VBoxManage list vms

Make sure it has an SSH server and a static ip first!

    VBoxHeadless -startvm <vm_name>


