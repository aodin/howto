Increase Virtual Box Hard Drive
===============================

Size in bytes:

    VBoxManage modifyhd <filename> --resize 10240

In `Storage`, navigate to the empty IDE controller. Click the CD icon and add an image that can run GParted.

The swap can be turned off with `swapoff -a`. 