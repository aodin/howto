Step 1
======

Download the tarfile that suits you best and extract it. Here’s the command to extract tar.bz2 files:

    tar xf Sublime\ Text\ 2.0.1\ x64.tar.bz2

You’ll notice that I got the 64-bit version. The reason is that it’s lightning fast. So, go for that if you can!


Step 2
======

You’ll get a “Sublime Text 2″ folder after extraction. This folder contains all the files that Sublime Text will need. So we have to move that folder somewhere more appropriate. Like the “/opt/” folder :

    sudo mv Sublime\ Text\ 2 /opt/


Step 3
======

At some point you’d want to be able to call Sublime Text from the Terminal by just typing “sublime”. To do that, we’ll just create a symbolic link in “/usr/bin” like thus:
    
    sudo ln -s /opt/Sublime\ Text\ 2/sublime_text /usr/bin/sublime


Step 4
======

Now that our files are at the right place, we need to create a launcher in Unity. To do this, we’re going to create a .desktop file in “/usr/share/applications”:

    sudo sublime /usr/share/applications/sublime.desktop

And paste the following content:

    [Desktop Entry]
    Version=1.0
    Name=Sublime Text 2
    # Only KDE 4 seems to use GenericName, so we reuse the KDE strings.
    # From Ubuntu's language-pack-kde-XX-base packages, version 9.04-20090413.
    GenericName=Text Editor
     
    Exec=sublime
    Terminal=false
    Icon=/opt/Sublime Text 2/Icon/48x48/sublime_text.png
    Type=Application
    Categories=TextEditor;IDE;Development
    X-Ayatana-Desktop-Shortcuts=NewWindow
     
    [NewWindow Shortcut Group]
    Name=New Window
    Exec=sublime -n
    TargetEnvironment=Unity

As you can see, these lines are quite straightforward. Go ahead and experiment a bit with them.


Step 5
======

Now you would probably want to open all text files with Sublime Text 2. The easiest way to do that is to open up the file associations list:

    sudo sublime /usr/share/applications/defaults.list

And replace all occurrences of gedit.desktop with sublime.desktop.


#### References:
[technoreply](http://www.technoreply.com/how-to-install-sublime-text-2-on-ubuntu-12-04-unity/)