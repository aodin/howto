
CompizConfig Settings Manager
=============================

sudo apt-get install compizconfig-settings-manager

# DO NOT RUN CCSM AS ROOT
ccsm 

Disable the keyboard shortcuts for Unity's switcher by unchecking CompizConfig Settings Manager ▸ Desktop ▸ Ubuntu Unity Plugin ▸ Switcher ▸ Key to start the switcher ▸ Enabled and Key to start the switcher in reverse ▸ Enabled

Enable the Static Application Switcher by checking CompizConfig Settings Manager ▸ Window Management ▸ Static Application Switcher ▸ Enable Static Application Switcher

#### References:
[askubuntu](http://askubuntu.com/a/68171)