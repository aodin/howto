Create a Remote Branch
======================

Use any branch that exists locally, ala:

    git checkout -b {branchname}

When pushed to the server under a new name, it will create a remote branch:

    git push origin {branchname}


Checkout a Remote Branch
========================

	git pull origin

The remote branch should now appear when you call:

	git branch -a

And can be checked out with

	git checkout {branchname}



Delete a Remote Branch
======================

From the local workspace:

    git branch -d {branchname}

From the remote repository:

    git push origin :{branchname}

OR

    git push origin --delete {branchname}

#### References:
[Stackoverflow](http://stackoverflow.com/a/2003515)


If the remote branch is still showing up on another workspace:

    git remote prune origin