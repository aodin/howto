Remove Added File Before Commit
===============================

	git reset FILENAME

OR

	git rm --cached FILENAME

#### References:
[stackoverflow](http://stackoverflow.com/questions/348170/undo-git-add-before-commit)