Install NumPy in a Virtual Environment
======================================

Install the dev packages of various libraries from the Ubuntu repository:

    libamd2.2.0 libblas3gf libc6 libgcc1 libgfortran3 liblapack3gf libumfpack5.4.0 libstdc++6 build-essential gfortran libatlas-dev libatlas3-base python python-all-dev gcc g++ libblas-dev liblapack-dev

We also want the GUI for `matplotlib`:

    libpng12-dev tcl-dev tk-dev python-tk